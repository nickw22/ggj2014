﻿using UnityEngine;
using System.Collections;

public class CharacterFader : MonoBehaviour {

	private bool isFading = false;

	private bool fadingOut = false;
	public float FadeTime;
	public float FadeWait;
	public GUITexture uiTexture;
	public GUIText uiText;
	private string selectedChar = "Happy";
	private AudioSource currentAudio;
	private AudioSource gotoAudio;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		float scaleAmount = 100.0f;
				if (isFading) {
			float fractionCompleted = Time.deltaTime / FadeTime;

					currentAudio.volume -= fractionCompleted / 2;
					gotoAudio.volume += fractionCompleted / 2;
						if (!fadingOut) {
								Color textureColor = uiTexture.color;
				textureColor.a += Mathf.Sin(fractionCompleted);
								uiTexture.color = textureColor;

								textureColor = uiText.color;
				textureColor.a += Mathf.Tan(fractionCompleted);
								uiText.color = textureColor;
				uiText.fontSize += (int)(scaleAmount * fractionCompleted);
								
				//audio
				if (uiTexture.color.a >= 1) {
			
										fadingOut = true;	
										
					gameObject.SendMessage("onCharacterFaded");
								}
						} else {

										Color textureColor = uiTexture.color;
					textureColor.a -= Mathf.Sin(fractionCompleted);
										uiTexture.color = textureColor;

										textureColor = uiText.color;
					textureColor.a -=  Mathf.Tan(fractionCompleted);
										uiText.color = textureColor;
					uiText.fontSize -= (int)(scaleAmount * fractionCompleted);
										if (uiTexture.color.a <= 0) {
												isFading = false;
										} 

						}
				}
		}
	public void StartFade(string charName)
	{
		Color textureColor = uiTexture.color;
		textureColor.a = 0;
		uiTexture.color = textureColor;
		textureColor = uiText.color;
		textureColor.a = 0;
		uiText.color = textureColor;
		uiText.text = charName;
		uiText.fontSize = 20;
		selectedChar = charName;
		isFading = true;
		fadingOut = false;
		foreach (AudioSource source in GetComponents<AudioSource>()) {
			if (source.volume > 0 && source.clip.name != "Emotions_" + charName + "_Music")
			{
				currentAudio = source;
				currentAudio	.volume = 1;
			}
			else if (source.clip.name == "Emotions_" + charName + "_Music")
			{
				gotoAudio = source;
				gotoAudio.volume = 0;
			}
		}
	}

	public string GetSelectedChar()
	{
		return selectedChar;
	}

	public bool IsFading()
	{
		return isFading;
		}
}
