﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterManager : MonoBehaviour {

	static CharacterManager instance;

	List<SwitchableCharacter> allCharacters = new List<SwitchableCharacter>();

	public static CharacterManager SharedInstance {
		get
		{
			if (instance == null) {
				instance = FindObjectOfType<CharacterManager>() as CharacterManager;
				if (instance == null) {
					GameObject obj = new GameObject ();
					obj.hideFlags = HideFlags.HideAndDontSave;
					instance = obj.AddComponent<CharacterManager>();
				}
			}
			return instance;
		}
	}

	// Use this for initialization


	public void registerCharacter(SwitchableCharacter character)
	{
		allCharacters.Add(character);
		if (allCharacters.Count == 3) {
			CharacterManager.SharedInstance.switchCharacter ("Happy");		
		}
	}

	public void switchCharacter(string charName)
	{
		for (int i = 0; i < allCharacters.Count; ++i) 
		{
			if (allCharacters[i].characterName == charName) 
			{
				allCharacters[i].EnableCharacter();
			}
			else
			{
				allCharacters[i].DisableCharacter();
			}
		}
	}


	void Start () {
	}
	
	// Update is called once per frame
			void Update ()
		{
				
		}
}
