﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour 
{

    //Events
	public delegate void PlayerPickedUpACollectible(GameObject sender);    
	public static event PlayerPickedUpACollectible OnPlayerPickedUpACollectible;

    private Transform _collectiblesParent;

	// Use this for initialization
    void OnEnable() 
    {
        if (_collectiblesParent==null) _collectiblesParent = GameObject.Find("CollectedCollectibles").transform;   
	}
	
	void OnTriggerEnter(Collider other)
    {
        transform.parent = _collectiblesParent;
        gameObject.SetActive(false);
		OnPlayerPickedUpACollectible (this.gameObject);
    }
}
