﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
	public float doorOpenTime = 10.0f;
	public float doorTranslationAmount = 20.0f;
	private float maxDoorHeight;
	private bool bRaiseDoor;
	private bool bDoorRaised;
	// Use this for initialization
	void Start () {
		maxDoorHeight = transform.position.y + doorTranslationAmount;
	}
	
	// Update is called once per frame
	void Update () {
		if (bRaiseDoor) {
			Vector3 position = transform.position;
			position.y += (Time.deltaTime / doorOpenTime) * doorTranslationAmount;
			transform.position = position;
			if ( transform.position.y >= maxDoorHeight)
			{
				bRaiseDoor = false;
				bDoorRaised = true;
			}
				}
	}

	void RaiseDoor()
	{
		if (!bDoorRaised) {
						bRaiseDoor = true;
				}
	}
}
