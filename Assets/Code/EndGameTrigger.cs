﻿using UnityEngine;
using System.Collections;

public class EndGameTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		//		Debug.Log("Player touching");
		if ( other.GetComponent<SwitchableCharacter>() != null)
		{
			SendMessageUpwards("EndGame");
			this.enabled = false;
		}
	}
}
