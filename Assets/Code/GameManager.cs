using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public Material skyboxAngry;
	public Material skyboxHappy;
	public Material skyboxSad;

	public List<GameObject> collectiblesCollected;
	private CharacterFader fader;
	private ScreenFader screenFader;
	void OnEnable() 
	{
		Collectible.OnPlayerPickedUpACollectible+=OnPlayerPickedUpACollectible;
	}
	
	void OnDisable() 
	{
		Collectible.OnPlayerPickedUpACollectible-=OnPlayerPickedUpACollectible;
	}

	void OnPlayerPickedUpACollectible(GameObject sender)
	{
		collectiblesCollected.Add (sender);
		GUIText collectedItems = transform.Find("NumItemsCollected").GetComponent<GUIText> ();
		collectedItems.text = "Orbs Collected: " + collectiblesCollected.Count + " of 3";
		if (collectiblesCollected.Count == 3) {
						BroadcastMessage ("RaiseDoor");
				}
	}

	void EndGame()
	{
		screenFader.FadeIn();
	}
	// Use this for initialization
	void Start () {
		//start in sad prefab
		transform.FindChild ("HappyPrefab").gameObject.SetActive(true);
		transform.FindChild ("AngryPrefab").gameObject.SetActive(false);
		transform.FindChild ("SadPrefab").gameObject.SetActive(false);
		fader = GetComponent<CharacterFader> ();
		screenFader = GetComponent<ScreenFader> ();
		RenderSettings.skybox = skyboxHappy;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Alpha3) )
		{
			if (!fader.IsFading())
			{
				if (Input.GetKeyDown (KeyCode.Alpha3) && fader.GetSelectedChar() != "Angry") {
					//angry
						fader.StartFade("Angry");

					} else if (Input.GetKeyDown (KeyCode.Alpha1) && fader.GetSelectedChar() != "Happy") {
					//happy
						fader.StartFade("Happy");
					} else if (Input.GetKeyDown (KeyCode.Alpha2) && fader.GetSelectedChar() != "Sad") {
					//sad
						fader.StartFade("Sad");
				}
			}
		}

 		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
	}

	public void onCharacterFaded()
	{
		if (fader.GetSelectedChar () == "Angry") {

			transform.FindChild("SadPrefab").gameObject.SetActive(false);
			transform.FindChild("HappyPrefab").gameObject.SetActive(false);
			transform.FindChild("AngryPrefab").gameObject.SetActive(true);		
			RenderSettings.skybox = skyboxAngry;
		}
		else if (fader.GetSelectedChar () == "Happy") {
			transform.FindChild("SadPrefab").gameObject.SetActive(false);
			transform.FindChild("HappyPrefab").gameObject.SetActive(true);
			transform.FindChild("AngryPrefab").gameObject.SetActive(false);
			RenderSettings.skybox = skyboxHappy;
				} else if (fader.GetSelectedChar () == "Sad") {
			transform.FindChild("SadPrefab").gameObject.SetActive(true);
			transform.FindChild("HappyPrefab").gameObject.SetActive(false);
			transform.FindChild("AngryPrefab").gameObject.SetActive(false);
			RenderSettings.skybox = skyboxSad;
				
				}
		CharacterManager.SharedInstance.switchCharacter (fader.GetSelectedChar ());
		foreach(InteractiveObject obj in GetComponentsInChildren<InteractiveObject>())
		{
			obj.Drop();
		}
		BroadcastMessage(fader.GetSelectedChar() + "Selected");
	}
}
