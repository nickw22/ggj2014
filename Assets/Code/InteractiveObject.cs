﻿using UnityEngine;
using System.Collections;

public class InteractiveObject : MonoBehaviour 
{
	private  GameObject playerGameObject;

	public GameObject triggerGameObject;

    public MoodState moodState;

	public GameObject AngryObject;
	public GameObject HappyObject;	
	public GameObject SadObject;

	private MoodState _previousMoodState;
	private GameObject _currentViewedObject;

	private bool _pickedUp;

	private bool _playerContact;

	private bool _acceptInput;
	private Rigidbody rigid;
	private bool	bRigidHasGravity;
    public enum MoodState
    {
        Angry,
        Happy,
        Sad
    }

	void OnEnable()
	{
		AngryObject.SetActive(false);
		HappyObject.SetActive(false);
		SadObject.SetActive(false);

		MoodStateChanged();

		_acceptInput = true;
		 
	}

	void PickUp()
	{
		_pickedUp = true;
		if (bRigidHasGravity) {
			rigid.useGravity = false;
		}
		Physics.IgnoreLayerCollision(8, 9);
		rigid.constraints = RigidbodyConstraints.FreezeRotation;
		StartCoroutine(InputTimer());
	}

	public void Drop()
	{
		_pickedUp = false;
		StartCoroutine(InputTimer());
		if (bRigidHasGravity) {
			rigid.useGravity = true;
		}
		rigid.constraints = RigidbodyConstraints.None;
		Physics.IgnoreLayerCollision(8, 9, false);
	}

	public float mCorrectionForce = 60.0f;
	
	public float mPointDistance = 3.0f;

	void Start ()
	{
		rigid = transform.GetComponent<Rigidbody> ();
		bRigidHasGravity = rigid.useGravity;
	}

	void AngrySelected()
	{
		moodState = MoodState.Angry;
	}

	void HappySelected()
	{
		moodState = MoodState.Happy;
	}

	void SadSelected()
	{
		moodState = MoodState.Sad;
	}
	// Update is called once per frame
	void Update () 
    {
		if (_acceptInput == true)
			CheckInput(); 

//		if (Input.GetMouseButtonDown(0) == true && _pickedUp == true)
//			Drop();

		if (_previousMoodState != moodState) MoodStateChanged();

		if (_pickedUp == true) 
		{
			Vector3 targetPoint = playerGameObject.GetComponentInChildren<Camera>().ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0));
			
			targetPoint += playerGameObject.GetComponentInChildren<Camera>().transform.forward * mPointDistance;
			
			Vector3 force = targetPoint - rigid.transform.position;		

			rigid.rigidbody.velocity = force.normalized * rigid.rigidbody.velocity.magnitude;
			rigid.rigidbody.AddForce(force * mCorrectionForce);
			rigid.rigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2);
		}
	}

	void CheckInput()
	{
		if (Input.GetMouseButtonDown(0)==true)
			{
				if (_pickedUp == false && _playerContact == true) PickUp();
				else Drop();
			}
	}

	void MoodStateChanged()
	{
		_previousMoodState = moodState;
		switch (moodState) 
		{
			case MoodState.Angry:
				{
					if (_currentViewedObject!=null) _currentViewedObject.SetActive(false);
					AngryObject.SetActive(true);
					_currentViewedObject = AngryObject;
					break;
				}
			case MoodState.Happy:
				{
					if (_currentViewedObject!=null) _currentViewedObject.SetActive(false);
					HappyObject.SetActive(true);
					_currentViewedObject = HappyObject;
					break;
				}
			case MoodState.Sad:
				{
					if (_currentViewedObject!=null) _currentViewedObject.SetActive(false);
					SadObject.SetActive(true);
					_currentViewedObject = SadObject;
					break;
				}

		}
	}

	void OnTriggerEnter(Collider other)
	{
//		Debug.Log("Player touching");
		if ( other.GetComponent<SwitchableCharacter>() != null)
		{
			playerGameObject = other.gameObject;
			_playerContact = true;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if ( other.GetComponent<SwitchableCharacter>() != null)
		{
			playerGameObject = other.gameObject;
			_playerContact = false;
		}
	}

	IEnumerator InputTimer()
	{
		_acceptInput = false;

		yield return new WaitForSeconds(1);

		_acceptInput = true;

		yield return null;
	}
}
