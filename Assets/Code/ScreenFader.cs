﻿using UnityEngine;
using System.Collections;

public class ScreenFader : MonoBehaviour {
	
	private bool isFading = false;
	
	private bool fadingOut = false;
	public float FadeTime;
	public float FadeWait;
	private float timeInFade;
	public GUITexture uiTexture;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isFading) {
			float fractionCompleted = Time.deltaTime / FadeTime;

			if (!fadingOut) {
				Color textureColor = uiTexture.color;
				textureColor.a += Mathf.Sin(fractionCompleted);
				uiTexture.color = textureColor;
				
				//audio
				if (uiTexture.color.a >= 1) {
					isFading = false;	
					gameObject.SendMessage("onScreenFaded");
				}
			} else {
				
				Color textureColor = uiTexture.color;
				textureColor.a -= Mathf.Sin(fractionCompleted);
				uiTexture.color = textureColor;

				if (uiTexture.color.a <= 0) {
					isFading = false;
				} 
				
			}
		}
	}
	public void FadeOut()
	{
		StartFade (EFadeType.FADEOUT);
		}
	public void FadeIn()
	{
		StartFade (EFadeType.FADEIN);
	}
	private void StartFade(EFadeType eFadeType)
	{
		Color textureColor = uiTexture.color;
		switch (eFadeType) {
		case EFadeType.FADEIN:
		{
			textureColor.a = 0;
			fadingOut = false;
		}
			break;
		case EFadeType.FADEOUT:
		{
			textureColor.a = 1;
			fadingOut = true;
		}
			break;
		}
		uiTexture.color = textureColor;
		isFading = true;

	}
	
	public bool IsFading()
	{
		return isFading;
	}

	private enum EFadeType
	{
		FADEIN,
		FADEOUT
	}
}


