using UnityEngine;
using System.Collections;

public class SwitchableCharacter : MonoBehaviour {

	public string characterName;
	// Use this for initialization
	void Start () 
	{
		CharacterManager.SharedInstance.registerCharacter (this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void EnableCharacter()
	{
		MouseLook mouseLook = GetComponent<MouseLook> ();
		mouseLook.enabled = true;
		this.gameObject.transform.Find(characterName + "Camera").gameObject.SetActive(true);
		MonoBehaviour CharacterMovement = this.gameObject.GetComponent ("CharacterMotor") as MonoBehaviour;
		CharacterMovement.Invoke ("EnableControl", 0);
		MonoBehaviour FPSInput = this.gameObject.GetComponent ("FPSInputController") as MonoBehaviour;
		Transform CharacterPlane = transform.FindChild ("Graphics").FindChild (characterName + "CharacterPlane");
		foreach (MeshRenderer renderer in CharacterPlane.GetComponentsInChildren<MeshRenderer> ()) {
			renderer.enabled = false;
		}
		FPSInput.enabled = true;

	}

	public void DisableCharacter()
	{
		MouseLook mouseLook = GetComponent<MouseLook> ();
		mouseLook.enabled = false;
		this.gameObject.transform.Find(characterName + "Camera").gameObject.SetActive(false);
		MonoBehaviour CharacterMovement = this.gameObject.GetComponent ("CharacterMotor") as MonoBehaviour;
		CharacterMovement.Invoke ("DisableControl", 0);
		//CharacterMovement.enabled = false;
		MonoBehaviour FPSInput = this.gameObject.GetComponent ("FPSInputController") as MonoBehaviour;
		Transform CharacterPlane = transform.FindChild ("Graphics").FindChild (characterName + "CharacterPlane");
		foreach (MeshRenderer renderer in CharacterPlane.GetComponentsInChildren<MeshRenderer> ()) {
						renderer.enabled = true;
				}
		FPSInput.enabled = false;

	}
}
