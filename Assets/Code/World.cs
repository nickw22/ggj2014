﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World: MonoBehaviour 
{

	public List<GameObject> collectiblesCollected;

	void OnEnable() 
	{
		Collectible.OnPlayerPickedUpACollectible+=OnPlayerPickedUpACollectible;
	}
	
	void OnDisable() 
	{
		Collectible.OnPlayerPickedUpACollectible-=OnPlayerPickedUpACollectible;
	}
	
	void OnPlayerPickedUpACollectible(GameObject sender)
	{
		collectiblesCollected.Add (sender);
	}
}
